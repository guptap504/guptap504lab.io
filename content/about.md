+++
title = "About Me"
slug = "about"
description = "about"
+++

Hi. I did my B.E. in Instrumentation and Control from [Netaji Subhas Institue of Technology](http://www.nsit.ac.in/) (now Netaji Subhas University of Technology).

I'm currently working at [EXL Services](https://www.exlservice.com/) as a Business Analyst. 

Here is my <a href="https://drive.google.com/open?id=1MEQgR3OGnlPcfzWOe8PzjnCdnCsp3ytz" target="_blank">resume</a>.

Like what you see? Send a DM on my [LinkedIn](https://www.linkedin.com/in/prashant-gupta-a75a39a5/).

Looking forward to work with you.